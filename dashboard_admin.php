<?php
include("conex.php");
$link=Conectarse();
if($_GET["admin"]!="50314815S"){
  header("Location: index.php");
}
# Enabling error display
error_reporting(E_ALL);
ini_set('display_errors', 1);
?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>
<script type='text/javascript' src='js/parallax.js'></script>

<!------ Include the above in your HEAD tag ---------->

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css2?family=Alegreya" rel="stylesheet" property="stylesheet" media="all" type="text/css" >
<link href="css/table.css" rel="stylesheet">
<link href="css/progress_bars.css" rel="stylesheet">

<script>
jQuery(document).ready(function($){
  // $.get("get_users.php", muestraResultadoGetUsers, "json");
  // $('#textbox1').val($(this).is(':checked'));

  $('.esencia').click(function() {
    // alert( $(this).val() );
    prepareUserChart($(this).val(), 'esencia');
  });

  $('.planDeVida').click(function() {
    // alert( $(this).val() );
    prepareUserChart($(this).val(), 'planDeVida');
  });

  $('.elementos').click(function() {
    // alert( $(this).val() );
    prepareUserChart($(this).val(), 'elementos');
  });

  $('.sombras').click(function() {
    // alert( $(this).val() );
    prepareUserChart($(this).val(), 'sombras');
  });

  $('.salud').click(function() {
    // alert( $(this).val() );
    prepareUserChart($(this).val(), 'salud');
  });

  $('.maestrias').click(function() {
    // alert( $(this).val() );
    prepareUserChart($(this).val(), 'maestrias');
  });

  $('.karmas').click(function() {
    // alert( $(this).val() );
    prepareUserChart($(this).val(), 'karmas');
  });

  $('.next').click(function() {
    // alert( $(this).val() );
    ponerAlFinalDeLaCola($(this).val());
  });

  $('.first').click(function() {
    // alert( $(this).val() );
    ponerAlPrincipiolDeLaCola($(this).val());
  });

  $('.enableLink').click(function() {
    // alert( $(this).val() );
    activarLink($(this).val());
  });

  $('.borrarUser').click(function() {
    // alert( $(this).val() );
    borrarUser($(this).val());
  });

});

function ponerAlFinalDeLaCola(id){
   $.get("to_last_pos.php?id="+id, muestraResultadoGetPonerAlFinal, "json");
      function muestraResultadoGetPonerAlFinal(respuesta){
        if (respuesta.success == 1){
          location.reload();
        }
      }
}

function ponerAlPrincipiolDeLaCola(id){
   $.get("to_first_pos.php?id="+id, muestraResultadoGetPonerAlPrincipio, "json");
      function muestraResultadoGetPonerAlPrincipio(respuesta){
        if (respuesta.success == 1){
          location.reload();
        }
      }
}

function activarLink(id){
  console.log(id);
   $.get("enable_link.php?id="+id, muestraResultadoGetEnableLink, "json");
      function muestraResultadoGetEnableLink(respuesta){
        console.log(respuesta)
        if (respuesta.lastLink == 1){
          console.log("link activado!, user: "+id);
        } else {
          console.log("link desactivado!, user: "+id);
        }
      }
}

function borrarUser(id){
  var r = confirm("¿Estás seguro de que desear borrar esta cuenta?");
  if (r == true) {
    $.get("borrar_userb.php?id="+id, muestraResultadoBorrarUser, "json");
      function muestraResultadoBorrarUser(respuesta){
        if (respuesta.success == 1){
          console.log("user borrado!, user: "+id);
          $("#tr_"+id).remove();
        } else {
          console.log("no pudo ser borrado!, user: "+id);
        }
      }
  } else {
    console.log("borrado candelado")
  }
}

function accentsTidy(s){
  var r=s.toLowerCase();
  r = r.replace(new RegExp("\\s", 'g'),"");
  r = r.replace(new RegExp("[àá]", 'g'),"a");
  r = r.replace(new RegExp("[èé]", 'g'),"e");
  r = r.replace(new RegExp("[ìí]", 'g'),"i");                           
  r = r.replace(new RegExp("[òó]", 'g'),"o");
  r = r.replace(new RegExp("[ùú]", 'g'),"u");
  r = r.replace(new RegExp("[ý]", 'g'),"y");
  return r;
};

function muestraResultadoGetMaestria(respuesta){
  console.log("i_val: "+respuesta.i_val)
  $("#maestria"+respuesta.i_val).html("A la edad de <span style='color:#b57615; font-size:2em'>"+respuesta.edad_cam+ "</span> alcanzas la maestría del <span style='color:#024457'>"+respuesta.maestria+"<span>");
}

function muestraResultadoGetKarma(respuesta){
  console.log("i_val: "+respuesta.i_val)
  $("#karma"+respuesta.i_val).html("A la edad de <span style='color:#921f1f; font-size:2em'>"+respuesta.edad_cam+ "</span> completas el Karma del <span style='color:#024457'>"+respuesta.karma+"<span>");
}

function prepareUserChart(id, topic){
  // console.log('id: '+id);
  // console.log($(".username_"+id).html());
  var usr = {};
  usr.name = accentsTidy($(".name_"+id).html()).toUpperCase();
  usr.sur1 = accentsTidy($(".surname1_"+id).html()).toUpperCase();
  usr.sur2 = accentsTidy($(".surname2_"+id).html()).toUpperCase();
  usr.day = $(".day_"+id).html();
  usr.month = $(".month_"+id).html();
  usr.year = $(".year_"+id).html();
  usr.born = $(".born_"+id).html();
  usr.feeling = $(".spirituality_"+id).html();

  console.log('user: '+JSON.stringify(usr));
  JSON.stringify(calcu(usr));


  if (topic=='esencia'){

    //--- GET ESENCIA ----//
    $.get("get_esencia.php?esencia="+usr.esencia+"&id="+id, muestraResultadoGetEsencia, "json");
    function muestraResultadoGetEsencia(respuesta){
      // // console.log(respuesta)
      $("#results").addClass("chart-normal-text").html("<span class='chart-centered-text'>ESENCIA</span><br>");
      $("#results").append("«La Esencia de tu Alma es la de una persona:<br><span id='esencia' class='chart-special-text'></span").append("</span>.»");
      $("#results").append("<br><span class='text-generic'>Pero esta es la esencia más profunda de tu alma. Ahora iremos viendo cuáles son el resto de energías que se manifiestan en tu Ser y en tu personalidad, y cuáles son tus desarmonizaciones así como los posibles problemas de salud derivados de estas.</span>");

       $("#esencia").html(respuesta.esencia);
    }

    //--- GET JUVENTUD ----//
    $.get("get_energia.php?energia="+usr.f4+'&aspect=1', muestraResultadoGetJuventud, "json");
      function muestraResultadoGetJuventud(respuesta){
        $("#results").addClass("chart-centered-text").append("<br><br><span class='chart-centered-text'>JUVENTUD</span><br>");
        $("#results").append('<span class="text-generic">Tu propósito de vida (el verdadero propósito por el que decidiste encarnar en este Mundo Terrenal) comienza realmente a los</span> <span class="activacion chart-special-text"></span><span  class="chart-special-text"> años</span>, <span class="text-generic">y hasta esa edad lo que has hecho es ir conformado la personalidad con la que has planeado vivir las experiencias que te ayudarán a alcanzar este propósito. Hasta los</span> <span class="activacion chart-special-text"></span> <span class="chart-special-text"> años</span> <span class="text-generic">te has caracterizado por ser una persona</span> <span id="juventud" class="chart-special-text"></span>.');

        $(".activacion").html(usr.activacion);  
        $("#juventud").html(respuesta.juventud);
      }

      //--- Update Logs ----//
      $.get("update_logs.php?id="+id+"&log=Es", muestraResultadoUpdateLogs, "json");
        function muestraResultadoUpdateLogs(respuesta){
        $(".log_"+id).html(respuesta.new_log);
      }
  }

  if (topic=='planDeVida'){

    //--- GET CAMINO ----//
    $.get("get_energia.php?energia="+usr.q1+'&aspect=2&feeling='+usr.feeling, muestraResultadoGetCamino, "json");
      function muestraResultadoGetCamino(respuesta){
      // // console.log(respuesta)
      $("#results").addClass("chart-normal-text").html("<span class='chart-centered-text'>TU CAMINO DE VIDA</span><br>");
      $("#results").append('<span class="text-generic">A lo largo de tu vida vas a experimentar muchas vivencias que ya estaban planificadas desde antes de que llegaras a este mundo terrenal, y que son las que, de una forma u otra, te harán transformarte poco a poco interiormente para que puedas llegar a realizar y completar tu propósito de vida. Si vives insconscientemente sufrirás. Para conseguir transitar por tu camino de vida de forma consciente deberás tener en cuenta que tu energía</span> <span id="camino" class="chart-special-text"></span>');

       $("#camino").html(respuesta.camino);
       $(".log_"+id).html(respuesta.new_log);
      }

    //--- GET PROPÓSITO ----//
    $.get("get_energia.php?energia="+usr.w4+'&aspect=3&feeling='+usr.feeling, muestraResultadoGetJuventud, "json");
      function muestraResultadoGetJuventud(respuesta){
        console.log('respuesta.activacion: '+respuesta.proposito)
        $("#results").addClass("chart-centered-text").append("<br><br><span class='chart-centered-text'>TU PROPÓSITO DE VIDA</span><br>");
        $("#results").append('<span class="text-generic">Has venido a este mundo a continuar con tu desarrollo espiritual</span> <span id="proposito" class="chart-special-text"></span>');

        $("#proposito").html(respuesta.proposito);  
      }

      //--- Update Logs ----//
      $.get("update_logs.php?id="+id+"&log=Pl", muestraResultadoUpdateLogs, "json");
        function muestraResultadoUpdateLogs(respuesta){
        $(".log_"+id).html(respuesta.new_log);
      }
  
  }

  //--- GET ELEMENTOS ----//
  if (topic=='elementos'){

      var arrayElements = [];
      arrayElements.push(usr.totalFuego);
      arrayElements.push(usr.totalAgua);
      arrayElements.push(usr.totalTierra);
      arrayElements.push(usr.totalAire);

      var totalAgua = Math.round(usr.totalAgua*100/usr.totalDivElems);
      var totalFuego = Math.round(usr.totalFuego*100/usr.totalDivElems);
      var totalTierra = Math.round(usr.totalTierra*100/usr.totalDivElems);
      var totalAire = Math.round(usr.totalAire*100/usr.totalDivElems);

      // // console.log(respuesta)
      $("#results").addClass("chart-normal-text").html("<br><span class='chart-centered-text'>LA FUERZA DE TUS ELEMENTOS</span><br><br>");
      $("#results").append('<span class="text-generic"><div class="wpsm_progress_b_row" id="wpsm_progress_b_row_12"><div class="col-md-12 col-sm-6"><div class="wpsm_progress"><div class="wpsm_progress-title" style="">FUEGO</div><div class="wpsm_progress-value" style=""><span id="elemFuego"></span>%</div><div class="wpsm_progress-pro-bar" style=""><div class="wpsm_progress-bar wow elemFuego" style="visibility: visible; animation-name: animate-positive;"></div></div></div></div><div class="col-md-12 col-sm-6"><div class="wpsm_progress"><div class="wpsm_progress-title" style="">AIRE</div><div class="wpsm_progress-value" style=""><span id="elemAire"></span>%</div><div class="wpsm_progress-pro-bar" style=""><div class="wpsm_progress-bar wow elemAire" style="visibility: visible; animation-name: animate-positive;"></div></div></div></div><div class="col-md-12 col-sm-6"><div class="wpsm_progress"><div class="wpsm_progress-title" style="">AGUA</div><div class="wpsm_progress-value" style=""><span id="elemAgua"></span>%</div><div class="wpsm_progress-pro-bar" style=""><div class="wpsm_progress-bar wow elemAgua" style="visibility: visible; animation-name: animate-positive;"></div></div></div></div><div class="col-md-12 col-sm-6"><div class="wpsm_progress"><div class="wpsm_progress-title" style="">TIERRA</div><div class="wpsm_progress-value" style=""><span id="elemTierra"></span>%</div><div class="wpsm_progress-pro-bar" style=""><div class="wpsm_progress-bar wow elemTierra" style="visibility: visible; animation-name: animate-positive;"></div></div></div></div></div><!-- END Progress Bars --></span>');

      $("#results").append("<br><span class='chart-centered-text'>¿Cuáles son las características de los 4 elementos?</span><br><br>");
      $("#results").append('<span class="text-generic"><p>Cada uno de los cuatro elementos tiene una serie de características comunes. Cuánto más fuertes estén presentes en ti, con mayor fuerza se manifestarán.</span><ul class="trx_addons_list_custom"><li>El <font style="color: #024457">FUEGO</font> es Luz, espíritu, sabiduría, actividad, lucha, batalla, calor, pasión, sexualidad…</li><li>El <font style="color: #024457">AIRE</font> es Consciencia, ideas, ideales, conocimiento, libertad, imaginación, fantasía…</li><li>El <font style="color: #024457">AGUA</font> es Inteligencia, amor, bondad, sentimiento, emoción, adaptabilidad, fluidez…</li><li>La <font style="color: #024457">TIERRA</font> es Estabilidad, solidez, seguridad, practicidad, materia, paciencia, serenidad…</li></ul>');

      $('#nombre_usuario').html(usr.name+' '+usr.sur1+' '+usr.sur2);
      $('#fecha_nacimiento_usuario').html(usr.day+'/'+usr.month+'/'+usr.year);
      $('#edad_usuario').html(usr.age);

      $('#elemFuego').html(totalFuego);
      $('#elemAgua').html(totalAgua);
      $('#elemTierra').html(totalTierra);
      $('#elemAire').html(totalAire);

      $('.elemFuego').css('width', totalFuego+'%');
      $('.elemAgua').css('width', totalAgua+'%');
      $('.elemTierra').css('width', totalTierra+'%');
      $('.elemAire').css('width', totalAire+'%');

      //--- Update Logs ----//
      $.get("update_logs.php?id="+id+"&log=El", muestraResultadoUpdateLogs, "json");
        function muestraResultadoUpdateLogs(respuesta){
        $(".log_"+id).html(respuesta.new_log);
      }
  }

  if (topic=='sombras'){

    //--- GET SOMBRAS ----//
      $("#results").addClass("chart-normal-text").html("<span class='chart-centered-text'>TUS POTENCIALES DEFECTOS</span><br>");
      $("#results").append('<span id="nivel_desarmonizaciones"></span><div class="elementor-element elementor-element-c2919b7 sc_fly_static elementor-widget elementor-widget-text-editor" data-id="c2919b7" data-element_type="widget" data-widget_type="text-editor.default"><div class="elementor-widget-container"><div class="elementor-text-editor elementor-clearfix" style="padding-top: 20px"><span id="defectos_container"><ul id="defectos" class="trx_addons_list_custom" style="color:#024457"></ul></span><span id="defectos_sep1"></span><span id="defectos2_container"><ul id="defectos2" class="trx_addons_list_custom" style="color:#024457"></ul></span><span id="defectos_sep2"></span><span id="defectos3_container"><ul id="defectos3" class="trx_addons_list_custom" style="color:#024457"></ul></span></div></div></div><span id="nivel_desarmonizaciones2"></span>');

      $.get("get_desarm.php?energia="+usr.numdesarmoniz+"&grav="+usr.gravedadnumdesarmoniz, muestraResultadoGetDesarm, "json");
        function muestraResultadoGetDesarm(respuesta){
          // // console.log('defectos: '+JSON.stringify(respuesta))
          if (respuesta.def3!=0){
            $( "<span>Puedes presentar en grado <span style='color:#024457'>moderado</span>, <span style='color:#024457'>alguno o varios</span> de los siguientes defectos:</span>" ).prependTo( "#defectos_container" );
            for (var i = 0; i < respuesta.def3.length; i++) {
              $('#defectos').append('<li>'+respuesta.def3[i]+'</li>');
            }
          } else {
            $('#defectos').html('');
          }
          if (respuesta.def2!=0){
            $( "<span>Puedes presentar en grado <span style='color:#024457'>grave</span>, <span style='color:#024457'>varios</span> de los siguientes defectos:</span>" ).prependTo( "#defectos2_container" );
            $('#defectos2').html('<ul class="trx_addons_list_custom" style="color:#024457"><li>'+respuesta.def2+'</li></ul>');
          } else {
            $('#defectos2').html('');
          }
          if (respuesta.def!=0){
            $( "<span>Puedes presentar en grado <span style='color:#024457'>muy grave</span>, <span style='color:#024457'>varios</span> de los siguientes defectos:</span>" ).prependTo( "#defectos3_container" );
            $('#defectos3').html('<ul class="trx_addons_list_custom" style="color:#024457"><li>'+respuesta.def+'</li></ul>');
          } else {
            $('#defectos3').html('');
          }

          if (respuesta.salud3!=0){
            $( "<span>En tu caso, puedes presentar en grado <span style='color:#024457'>moderado</span>, <span style='color:#024457'>alguno o varios</span> de los siguientes problemas de salud:</span>" ).prependTo( "#problemas_salud_container" );
            for (var i = 0; i < respuesta.salud3.length; i++) {
              $('#salud').append('<li>'+respuesta.salud3[i]+'</li>');
            }
          } else {
            $('#salud').html('');
          }
          if (respuesta.salud2!=0){
            $( "<span>En tu caso, puedes presentar en grado <span style='color:#024457'>grave</span>, <span style='color:#024457'>varios</span> de los siguientes problemas de salud:</span>" ).prependTo( "#problemas_salud2_container" );
            $('#salud2').html('<ul class="trx_addons_list_custom" style="color:#024457"><li>'+respuesta.salud2+'</li></ul>');
          } else {
            $('#salud2').html('');
          }
          if (respuesta.salud!=0){
            $( "<span>En tu caso, puedes presentar en grado <span style='color:#024457'>muy grave</span>, <span style='color:#024457'>varios</span> de los siguientes problemas de salud:</span>" ).prependTo( "#defectos3_container_container" );
            $('#salud3').html('<ul class="trx_addons_list_custom" style="color:#024457"><li>'+respuesta.salud+'</li></ul>');
          } else {
            $('#salud3').html('');
          }

        }

      //--- Update Logs ----//
      $.get("update_logs.php?id="+id+"&log=So", muestraResultadoUpdateLogs, "json");
        function muestraResultadoUpdateLogs(respuesta){
        $(".log_"+id).html(respuesta.new_log);
      }
  }

  if (topic=='salud'){

    //--- GET SALUD ----//
      // // console.log(respuesta)
      $("#results").addClass("chart-normal-text").html("<span class='chart-centered-text'>POSIBLES PROBLEMAS DE SALUD</span><br>");
      $("#results").append('<span class="text-generic">Los problemas de salud están relacionados con las desarmonizaciones presentes en nuestras energías.</span><div class="elementor-text-editor elementor-clearfix" style="padding-top: 20px"><span id="problemas_salud_container"><ul id="salud" class="trx_addons_list_custom" style="color:#024457"></ul></span>        <span id="problemas_sep1"></span>        <span id="problemas_salud2_container"><ul id="salud2" class="trx_addons_list_custom" style="color:#024457"></ul></span>        <span id="problemas_sep2"></span>        <span id="problemas_salud3_container"><ul id="salud3" class="trx_addons_list_custom" style="color:#024457"></ul></span></div>');
      
      $.get("get_desarm.php?energia="+usr.numdesarmoniz+"&grav="+usr.gravedadnumdesarmoniz, muestraResultadoGetDesarm, "json");
        function muestraResultadoGetDesarm(respuesta){
          // // console.log('defectos: '+JSON.stringify(respuesta))
          if (respuesta.def3!=0){
            $( "<span>Puedes presentar en grado <span style='color:#024457'>moderado</span>, <span style='color:#024457'>alguno o varios</span> de los siguientes defectos:</span>" ).prependTo( "#defectos_container" );
            for (var i = 0; i < respuesta.def3.length; i++) {
              $('#defectos').append('<li>'+respuesta.def3[i]+'</li>');
            }
          } else {
            $('#defectos').html('');
          }
          if (respuesta.def2!=0){
            $( "<span>Puedes presentar en grado <span style='color:#024457'>grave</span>, <span style='color:#024457'>varios</span> de los siguientes defectos:</span>" ).prependTo( "#defectos2_container" );
            $('#defectos2').html('<ul class="trx_addons_list_custom" style="color:#024457"><li>'+respuesta.def2+'</li></ul>');
          } else {
            $('#defectos2').html('');
          }
          if (respuesta.def!=0){
            $( "<span>Puedes presentar en grado <span style='color:#024457'>muy grave</span>, <span style='color:#024457'>varios</span> de los siguientes defectos:</span>" ).prependTo( "#defectos3_container" );
            $('#defectos3').html('<ul class="trx_addons_list_custom" style="color:#024457"><li>'+respuesta.def+'</li></ul>');
          } else {
            $('#defectos3').html('');
          }

          if (respuesta.salud3!=0){
            $( "<span>En tu caso, puedes presentar en grado <span style='color:#024457'>moderado</span>, <span style='color:#024457'>alguno o varios</span> de los siguientes problemas de salud:</span>" ).prependTo( "#problemas_salud_container" );
            for (var i = 0; i < respuesta.salud3.length; i++) {
              $('#salud').append('<li>'+respuesta.salud3[i]+'</li>');
            }
          } else {
            $('#salud').html('');
          }
          if (respuesta.salud2!=0){
            $( "<span>En tu caso, puedes presentar en grado <span style='color:#024457'>grave</span>, <span style='color:#024457'>varios</span> de los siguientes problemas de salud:</span>" ).prependTo( "#problemas_salud2_container" );
            $('#salud2').html('<ul class="trx_addons_list_custom" style="color:#024457"><li>'+respuesta.salud2+'</li></ul>');
          } else {
            $('#salud2').html('');
          }
          if (respuesta.salud!=0){
            $( "<span>En tu caso, puedes presentar en grado <span style='color:#024457'>muy grave</span>, <span style='color:#024457'>varios</span> de los siguientes problemas de salud:</span>" ).prependTo( "#defectos3_container_container" );
            $('#salud3').html('<ul class="trx_addons_list_custom" style="color:#024457"><li>'+respuesta.salud+'</li></ul>');
          } else {
            $('#salud3').html('');
          }

        }

      //--- Update Logs ----//
      $.get("update_logs.php?id="+id+"&log=Sa", muestraResultadoUpdateLogs, "json");
        function muestraResultadoUpdateLogs(respuesta){
        $(".log_"+id).html(respuesta.new_log);
      }
  
  }

  if (topic=='maestrias'){

    //--- GET MAESTRÍAS ----//

      $("#results").addClass("chart-normal-text").html("<span class='chart-centered-text'>MAESTRÍAS/TALENTOS</span><br>");
      $("#results").append('<span class="text-generic">Estas son las áreas de la vida en las que la persona tiene sus <span style="color:#024457;">mayores habilidades</span>, en el cuadro de al lado aparecen las edades previstas de activación (siguiendo el mismo orden). Esto indica que a partir de esa edad las enseñanzas de ese número ya se dominan (si no habido bloqueos excesivos), por lo que una maestría en un cierto número hace que a partir de la edad señalada este riesgo acabe siendo prácticamente inexistente. Aunque cabe recordar que uno no es “maestro” hasta que ejerce como tal.</span>        <div class="elementor-text-editor elementor-clearfix" style="padding-top: 20px">        <span id="maestria0_container"><ul id="maestria0" class="trx_addons_list_custom"></ul></span>        <span id="maestria_sep1"></span>        <span id="maestria1_container"><ul id="maestria1" class="trx_addons_list_custom"></ul></span>        <span id="maestria_sep2"></span>        <span id="maestria2_container"><ul id="maestria2" class="trx_addons_list_custom"></ul></span>        <span id="maestria_sep3"></span>        <span id="maestria3_container"><ul id="maestria3" class="trx_addons_list_custom"></ul></span>        <span id="maestria_sep4"></span>        <span id="maestria4_container"><ul id="maestria4" class="trx_addons_list_custom"></ul></span>        <span id="maestria_sep5"></span>        <span id="maestria5_container"><ul id="maestria5" class="trx_addons_list_custom"></ul></span>        <span id="maestria_sep6"></span>        <span id="maestria6_container"><ul id="maestria6" class="trx_addons_list_custom"></ul></span>        <span id="maestria_sep7"></span>        <span id="maestria7_container"><ul id="maestria7" class="trx_addons_list_custom"></ul></span>        <span id="maestria_sep8"></span>        <span id="maestria8_container"><ul id="maestria8" class="trx_addons_list_custom"></ul></span>        <span id="maestria_sep9"></span>        <span id="maestria9_container"><ul id="maestria9" class="trx_addons_list_custom"></ul></span></div>');

      //--- GET MAESTRÍAS y KARMAS ----//
      for (var i = 0; i < usr.z1.length; i++){
        var edadCam = parseInt(usr.edadesCamino[i]);
        if (usr.valoresTotalesNumbers.indexOf(usr.z1[i])!==-1){
          // es MAESTRÍA
          $.get("get_maestria.php?maestria="+usr.z1[i]+"&i_val="+i+"&edad_cam="+edadCam, muestraResultadoGetMaestria, "json");  
        } else{
          // es KÁRMICO
          $.get("get_karma.php?karma="+usr.z1[i]+"&i_val="+i+"&edad_cam="+edadCam, muestraResultadoGetKarma, "json");
        }
      }

      for (var i = 0; i < usr.z2.length; i++){
        var edadCam = parseInt(usr.edadesCamino[i+3]);
        var i_val = i+3;
        if (usr.valoresTotalesNumbers.indexOf(usr.z2[i])!==-1){
          // es MAESTRÍA
          $.get("get_maestria.php?maestria="+usr.z2[i]+"&i_val="+i_val+"&edad_cam="+edadCam, muestraResultadoGetMaestria, "json");
        } else{
          // es KÁRMICO
          $.get("get_karma.php?karma="+usr.z2[i]+"&i_val="+i_val+"&edad_cam="+edadCam, muestraResultadoGetKarma, "json");
        } 
      }

      for (var i = 0; i < usr.z3.length; i++){
        var edadCam = parseInt(usr.edadesCamino[i+6]);
        var i_val = i+6;
        if (usr.valoresTotalesNumbers.indexOf(usr.z3[i])!==-1){
          // es MAESTRÍA
          $.get("get_maestria.php?maestria="+usr.z3[i]+"&i_val="+i_val+"&edad_cam="+edadCam, muestraResultadoGetMaestria, "json");
        } else{
          // es KÁRMICO
          $.get("get_karma.php?karma="+usr.z3[i]+"&i_val="+i_val+"&edad_cam="+edadCam, muestraResultadoGetKarma, "json");
        } 
      }

      //--- Update Logs ----//
      $.get("update_logs.php?id="+id+"&log=Ma", muestraResultadoUpdateLogs, "json");
        function muestraResultadoUpdateLogs(respuesta){
        $(".log_"+id).html(respuesta.new_log);
      }
  
  }

  if (topic=='karmas'){

    //--- GET KARMAS ----//

      $("#results").addClass("chart-normal-text").html("<span class='chart-centered-text'>KARMAS</span><br>");
      $("#results").append('<span class="text-generic">Los números Kármicos tienen el mismo potencial que los números de Maestría, sólo que su conversión a números dhármicos (maestros) se obtiene a partir de una serie de experiencias asociadas a la dificultad (conflictos familiares, miedos, apegos, carencias…). Sin embargo, una vez que esas vivencias han sido superadas de manera positiva, la energía de ese número queda transmutada en Luz y su experiencia puede ser de gran ayuda para otros que se encuentren en situaciones parecidas.</span><div class="elementor-text-editor elementor-clearfix" style="padding-top: 20px">        <span id="karma0_container"><ul id="karma0" class="trx_addons_list_custom"></ul></span>        <span id="karma_sep1"></span>        <span id="karma1_container"><ul id="karma1" class="trx_addons_list_custom"></ul></span>        <span id="karma_sep2"></span>        <span id="karma2_container"><ul id="karma2" class="trx_addons_list_custom"></ul></span>        <span id="karma_sep3"></span>        <span id="karma3_container"><ul id="karma3" class="trx_addons_list_custom"></ul></span>        <span id="karma_sep4"></span>        <span id="karma4_container"><ul id="karma4" class="trx_addons_list_custom"></ul></span>        <span id="karma_sep5"></span>        <span id="karma5_container"><ul id="karma5" class="trx_addons_list_custom"></ul></span>        <span id="karma_sep6"></span>        <span id="karma6_container"><ul id="karma6" class="trx_addons_list_custom"></ul></span>        <span id="karma_sep7"></span>        <span id="karma7_container"><ul id="karma7" class="trx_addons_list_custom"></ul></span>        <span id="karma_sep8"></span>        <span id="karma8_container"><ul id="karma8" class="trx_addons_list_custom"></ul></span>        <span id="karma_sep9"></span>        <span id="karma9_container"><ul id="karma9" class="trx_addons_list_custom"></ul></span></div>');

      //--- GET MAESTRÍAS y KARMAS ----//
      for (var i = 0; i < usr.z1.length; i++){
        var edadCam = parseInt(usr.edadesCamino[i]);
        if (usr.valoresTotalesNumbers.indexOf(usr.z1[i])!==-1){
          // es MAESTRÍA
          $.get("get_maestria.php?maestria="+usr.z1[i]+"&i_val="+i+"&edad_cam="+edadCam, muestraResultadoGetMaestria, "json");  
        } else{
          // es KÁRMICO
          $.get("get_karma.php?karma="+usr.z1[i]+"&i_val="+i+"&edad_cam="+edadCam, muestraResultadoGetKarma, "json");
        }
      }

      for (var i = 0; i < usr.z2.length; i++){
        var edadCam = parseInt(usr.edadesCamino[i+3]);
        var i_val = i+3;
        if (usr.valoresTotalesNumbers.indexOf(usr.z2[i])!==-1){
          // es MAESTRÍA
          $.get("get_maestria.php?maestria="+usr.z2[i]+"&i_val="+i_val+"&edad_cam="+edadCam, muestraResultadoGetMaestria, "json");
        } else{
          // es KÁRMICO
          $.get("get_karma.php?karma="+usr.z2[i]+"&i_val="+i_val+"&edad_cam="+edadCam, muestraResultadoGetKarma, "json");
        } 
      }

      for (var i = 0; i < usr.z3.length; i++){
        var edadCam = parseInt(usr.edadesCamino[i+6]);
        var i_val = i+6;
        if (usr.valoresTotalesNumbers.indexOf(usr.z3[i])!==-1){
          // es MAESTRÍA
          $.get("get_maestria.php?maestria="+usr.z3[i]+"&i_val="+i_val+"&edad_cam="+edadCam, muestraResultadoGetMaestria, "json");
        } else{
          // es KÁRMICO
          $.get("get_karma.php?karma="+usr.z3[i]+"&i_val="+i_val+"&edad_cam="+edadCam, muestraResultadoGetKarma, "json");
        } 
      }

      //--- Update Logs ----//
      $.get("update_logs.php?id="+id+"&log=Ka", muestraResultadoUpdateLogs, "json");
        function muestraResultadoUpdateLogs(respuesta){
        $(".log_"+id).html(respuesta.new_log);
      }
  
  }

}

</script>
</head>
<body>

<style>
.container {
    max-width: max-content;
}
</style>

<div class="container">
  <div class="row">

    <div class="col-4">
      <div id="results"></div>
    </div> <!-- class="col-4" -->

    <div class="col-8">

    <h1>Dashboard <span>1.0</span> de <span>Numerología</span></h1>

    <table id="users" class="responstable">
      
      <tr>
        <th>Usuario</th>
        <th data-th="Info"><span>Nombre</span></th>
        <th>Apellidos</th>
        <th>Edad</th>
        <th>Fecha de nacimiento</th>
        <th>Log</th>
        <th>Esencia y activación</th>
        <th>Plan y Propósito</th>
        <th>Elementos</th>
        <th>Sombras</th>
        <th>Salud</th>
        <th>Maestrías</th>
        <th>Karmas</th>
        <th>Pasar de ronda</th>
        <th>Poner primero</th>
        <th>Activar Link</th>
        <th>Borrar cuenta</th>
      </tr>
      
      <?php

      // Para calcular fecha de nacimiento
        $hoy = new DateTime();
        $sq_find_users = "SELECT * FROM users ORDER BY last_active ASC";
        $r_find_users = mysql_query($sq_find_users,$link);

        while ($row_find_users = mysql_fetch_array($r_find_users)) {
          $surname = $row_find_users["surname1"].' '.$row_find_users["surname2"];
          $birthdate = $row_find_users["day"].'/'.$row_find_users["month"].'/'.$row_find_users["year"];
          // echo $row_find_users["day"].'-'.$row_find_users["month"].'-'.$row_find_users["year"];
          $fecha_nacimiento = new DateTime($row_find_users["day"].'-'.$row_find_users["month"].'-'.$row_find_users["year"]);
          $age = $hoy->diff($fecha_nacimiento);
          if ($row_find_users["link_enabled"]==0){
            $link_type = "<td><input class='enableLink' type='checkbox' name='radio-button' value='".$row_find_users["id"]."'/></td>";       
          } else {
            $link_type = "<td><input class='enableLink' type='checkbox' name='radio-button' value='".$row_find_users["id"]."' checked/></td>";   
          }
               echo "
               <tr id='tr_".$row_find_users["id"]."'>
                <td><span class='username_".$row_find_users["id"]."'>".$row_find_users["user"]."</span></td>
                <td><span class='name_".$row_find_users["id"]."'>".$row_find_users["name"]."</span></td>
                <td><span class='surname_".$row_find_users["id"]."_".$row_find_users["id"]."'>".$surname."</span></td>
                <td>".$age->y."</td>
                <td hidden><span class='surname1_".$row_find_users["id"]."'>".$row_find_users["surname1"]."</span></td>
                <td hidden><span class='surname2_".$row_find_users["id"]."'>".$row_find_users["surname2"]."</span></td>
                <td><span class='birthdate_".$row_find_users["id"]."'>".$birthdate."</span></td>
                <td><span class='log_".$row_find_users["id"]."'>".$row_find_users["log"]."</span></td>
                <td hidden><span class='day_".$row_find_users["id"]."'>".$row_find_users["day"]."</span></td>
                <td hidden><span class='month_".$row_find_users["id"]."'>".$row_find_users["month"]."</span></td>
                <td hidden><span class='year_".$row_find_users["id"]."'>".$row_find_users["year"]."</span></td>
                <td hidden><span class='born_".$row_find_users["id"]."'>".$row_find_users["born"]."</span></td>
                <td hidden><span class='spirituality_".$row_find_users["id"]."'>".$row_find_users["spirituality"]."</span></td>
                <td><input class='esencia' type='radio' name='radio-button' value='".$row_find_users["id"]."' /></td>
                <td><input class='planDeVida' type='radio' name='radio-button' value='".$row_find_users["id"]."'/></td>

                <td><input class='elementos' type='radio' name='radio-button' value='".$row_find_users["id"]."'/></td>
                <td><input class='sombras' type='radio' name='radio-button' value='".$row_find_users["id"]."'/></td>
                <td><input class='salud' type='radio' name='radio-button' value='".$row_find_users["id"]."'/></td>
                <td><input class='maestrias' type='radio' name='radio-button' value='".$row_find_users["id"]."'/></td>
                <td><input class='karmas' type='radio' name='radio-button' value='".$row_find_users["id"]."'/></td>

                <td><input class='next' type='radio' name='radio-button' value='".$row_find_users["id"]."'/></td>
                <td><input class='first' type='radio' name='radio-button' value='".$row_find_users["id"]."'/></td>
                ".$link_type."
                <td><input class='borrarUser' type='radio' name='radio-button' value='".$row_find_users["id"]."'/></td>
              </tr>";
        }

      ?>

    </table>

    </div> <!-- class="col-8" -->

</div> <!-- row -->
</div> <!-- container -->

</body>
</html>