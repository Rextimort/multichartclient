<?php
/* Form Page
* Formulario para solicitar tu Carta Numerológica.
* @author : Gonzalo Pareja Bolaños
* @copyright : Youtube Channel: Practicante Espiritual
*/

# Enabling error display
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

# Including all the required scripts for demo
require __DIR__ . "/includes/functions.php";
require __DIR__ . "/includes/discord.php";
require __DIR__ . "/config.php";

# ALL VALUES ARE STORED IN SESSION!
# RUN `echo var_export([$_SESSION]);` TO DISPLAY ALL THE VARIABLE NAMES AND VALUES.
# FEEL FREE TO JOIN MY SERVER FOR ANY QUERIES - https://join.markis.dev

if (isset($_GET['code']) && isset($_GET['state']) && !isset($_SESSION['access_token'])){
	init($redirect_url, $client_id, $secret_id);
	get_user();
	join_guild("808729442398109746");
} else if ($_GET['edit_data']==1){
 // Nos quedamos editando
} else {
	// ha metido la url a mano
	//header("Location: index.php");
}

include("conex.php");
$link=Conectarse();

if (isset($_SESSION['user_id']) != ''){
	if (isset($_SESSION['auth']) && $_GET['edit_data'] != 1){
		// echo "PAAA";
		header('Location: http://localhost:8888/numprojectclassic/dashboard.php');
	} else if (isset($_SESSION['auth']) && $_GET['edit_data'] == 1){
		// te quedas editando
		// echo "PEEE";
	} else {
		// no tiene sesión de user activa
		// echo "PIII";
		// echo $_SESSION['user_id'];
		$sq_check_db = "SELECT * FROM users WHERE user_id='".$_SESSION['user_id']."'";
		$r_check_db = mysql_query($sq_check_db,$link);
			if (mysql_num_rows($r_check_db)==1){
				$tupla_check_db=mysql_fetch_array($r_check_db);
				$name=$tupla_check_db["name"];
				$surname1=$tupla_check_db["surname1"];
				$surname2=$tupla_check_db["surname2"];
				$day=$tupla_check_db["day"];
				$month=$tupla_check_db["month"];
				$year=$tupla_check_db["year"];
				$born=$tupla_check_db["born"];
				$spirituality=$tupla_check_db["spirituality"];
					$_SESSION['name'] = $name;
					$_SESSION['surname1'] = $surname1;
					$_SESSION['surname2'] = $surname2;
					$_SESSION['fullname'] = $name.' '.$surname1.' '.$surname2;
					$_SESSION['birthdate'] = $day."/".$month."/".$year;
					$_SESSION['country'] = $born;
					$_SESSION['spirituality'] = $spirituality;
					$_SESSION['auth'] = true;
					//echo "PEEEE";
				header('Location: '.$server_url.'dashboard.php');
			} else {
				echo "Ese user fue borrado. Tendrás que introducir tus datos de nuevo en el formulario.";
			}

	}
} else {
	// no está logeado en discord
	header("Location: index.php");
}

// Esta parte es por si el user cierra la app y luego quiere entrar Y YA TIENE UN USER en la BBDD
/*
if (isset($_SESSION['user_id']) != ''){
	echo "III";
	echo $_GET['edit_data'];
	echo $_SESSION['name'];
	// Check if user is in DB
	if ($_SESSION['auth'] && $_GET['edit_data'] != 1){
		echo "OOOO";
		$sq_check_db = "SELECT user_id FROM users WHERE user_id='".$_SESSION['user_id']."'";
		$r_check_db = mysql_query($sq_check_db,$link);
			if (mysql_num_rows($r_check_db)==1){
				$tupla_check_db=mysql_fetch_array($r_check_db);
				$name=$tupla_check_db["name"];
				$surname1=$tupla_check_db["surname1"];
				$surname2=$tupla_check_db["surname2"];
				$day=$tupla_check_db["day"];
				$month=$tupla_check_db["month"];
				$year=$tupla_check_db["year"];
				$born=$tupla_check_db["born"];
				$spirituality=$tupla_check_db["spirituality"];
					$_SESSION['name'] = $name;
					$_SESSION['surname1'] = $surname1;
					$_SESSION['surname2'] = $surname2;
					$_SESSION['fullname'] = $name.' '.$surname1.' '.$surname2;
					$_SESSION['birthdate'] = $day."/".$month."/".$year;
					$_SESSION['country'] = $born;
					$_SESSION['spirituality'] = $spirituality;
					echo "PEEEE";
				header('Location: '.$server_url.'dashboard.php');
			} else {
				// ese user_id fue borrado de la BBDD
			}
	} else if (isset($_SESSION['name']) != '' && $_GET['edit_data'] == 1){
		// no hago nada y le presento el formulario
		echo "PUUU";
	}

} else {
	// no está logeado en discord
	echo "PXXX";
	header("Location: index.php");
}
*/
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/style.css">
<link href="css/main.css" rel="stylesheet">
<link href="css/popup.css" rel="stylesheet">

<script>

	function inputValidate(strValue) {
	  strValue = strValue.replace(/\s/g,'')
	  // console.log('strValue: '+strValue)
	  var objRegExp  = /^[A-Za-zÑñàáèéìíòóùúșăâîțÀÁÈÉÌÍÒÓÙÚȘĂÂÎȚ]+$/;
	  // var objRegExp  = "[a-zA-Z\u00f1\u00d1]*"
	  return objRegExp.test(strValue);
	}

	function accentsTidy(s){
            var r=s.toLowerCase();
            r = r.replace(new RegExp("\\s", 'g'),"");
            r = r.replace(new RegExp("[àá]", 'g'),"a");
            r = r.replace(new RegExp("[èé]", 'g'),"e");
            r = r.replace(new RegExp("[ìí]", 'g'),"i");                           
            r = r.replace(new RegExp("[òó]", 'g'),"o");
            r = r.replace(new RegExp("[ùú]", 'g'),"u");
            r = r.replace(new RegExp("[ý]", 'g'),"y");
            // r = r.replace(new RegExp("\\W", 'g'),"");
           /* 
            r = r.replace(new RegExp("\\s", 'g'),"");
            r = r.replace(new RegExp("[àáâãäå]", 'g'),"a");
            r = r.replace(new RegExp("æ", 'g'),"ae");
            r = r.replace(new RegExp("ç", 'g'),"c");
            r = r.replace(new RegExp("[èéêë]", 'g'),"e");
            r = r.replace(new RegExp("[ìíîï]", 'g'),"i");
            r = r.replace(new RegExp("ñ", 'g'),"n");                            
            r = r.replace(new RegExp("[òóôõö]", 'g'),"o");
            r = r.replace(new RegExp("œ", 'g'),"oe");
            r = r.replace(new RegExp("[ùúûü]", 'g'),"u");
            r = r.replace(new RegExp("[ýÿ]", 'g'),"y");
            r = r.replace(new RegExp("\\W", 'g'),"");
           */
            return r;
    };

	function validateForm(){


		if (!$('[name=your-name]').val().trim() || !inputValidate($('[name=your-name]').val()) ){
    		form_error = 1;
    		form_error_msg = "Debes introducir el Nombre";
    		$('[name=your-name]').css({'outline': '2px solid orange'});
    		return form_error_msg;
    	} else if (!$('[name=your-surname1]').val().trim() || !inputValidate($('[name=your-surname1]').val()) ){
    		form_error = 1;
    		form_error_msg = "Debes introducir el primer apellido";
    		$('[name=your-surname1]').css({'outline': '2px solid orange'});
    		$('[name=your-name]').removeAttr("style");
    		return form_error_msg;
    	} else if (!$('[name=your-surname2]').val().trim() || !inputValidate($('[name=your-surname2]').val()) ){
    		form_error = 1;
    		form_error_msg = "Debes introducir el segundo apellido";
    		$('[name=your-surname2]').css({'outline': '2px solid orange'});
    		$('[name=your-surname1]').removeAttr("style");
    		return form_error_msg;
    	} else if ($('[name=Born]').val() == ''){
    		form_error = 1;
    		// form_error_msg = "Debes seleccionar el País de nacimiento";
    		$('[name=Born]').css({'outline': '2px solid orange'});
    		$('[name=your-surname2]').removeAttr("style");
    		return form_error_msg;
    	} else if ($('[name=Day]').val() == ''){
    		form_error = 1;
    		form_error_msg = "Debes seleccionar el Día de nacimiento";
    		$('[name=Day]').css({'outline': '2px solid orange'});
    		$('[name=Born]').removeAttr("style");
    		return form_error_msg;
    	} else if ($('[name=month]').val() == ''){
    		form_error = 1;
    		form_error_msg = "Debes seleccionar el Mes de nacimiento";
    		$('[name=month]').css({'outline': '2px solid orange'});
    		$('[name=Day]').removeAttr("style");
    		return form_error_msg;
    	} else if ($('[name=Year]').val() == ''){
    		form_error = 1;
    		form_error_msg = "Debes seleccionar el Año de nacimiento";
    		$('[name=Year]').css({'outline': '2px solid orange'});
    		$('[name=month]').removeAttr("style");
    		return form_error_msg;
    	}else if ($('[name=feeling]').val() == ''){
    		form_error = 1;
    		form_error_msg = "Debes responder a la pregunta";
    		$('[name=feeling]').css({'outline': '2px solid orange'});
    		$('[name=Year]').removeAttr("style");
    		return form_error_msg;
    	}else if (!$('[name=acceptance]').is(":checked")){
    		form_error = 1;
    		form_error_msg = "Debes aceptar los términos de uso";
    		$('.acceptance-label').css({'outline': '2px solid orange'});
    		$('[name=feeling]').removeAttr("style");
    		return form_error_msg;
    	} else {
    		form_error_msg = false;
    		form_error = 0;
    		return form_error_msg;
    	}

	}
	$(document).ready(function(){

		$("#register").click(function(){
			
			var formResult = validateForm();
			
			if (!formResult){
				console.log('el form se rellenó sin errores');
				
				// Salta el overlay
				$('.content-popup').css("box-shadow", "none");
				$('.popup-overlay').css("opacity", 0.9);
				$('.popup-overlay').fadeIn('slow');
		        $('.popup-overlay').height($(document).height());
		        $('#popup').show();
				$('.preloader-popup').show();

				var usr = {};
				usr.username = $('[name=your-username]').val();
				usr.user_id = $('[name=your-userid]').val();
		    	usr.name = accentsTidy($('[name=your-name]').val()).toUpperCase();
		    	usr.sur1 = accentsTidy($('[name=your-surname1]').val()).toUpperCase();
		    	usr.sur2 = accentsTidy($('[name=your-surname2]').val()).toUpperCase();
		    	usr.day = $('[name=Day]').val();
		    	usr.month = $('[name=month]').val();
		    	usr.year = $('[name=Year]').val();
		    	usr.born = $('[name=Born]').val();
		    	usr.feeling = $('[name=feeling]').val();

		    	console.log(JSON.stringify(usr));

				setTimeout(function(){
					// Incribo los datos y paso página
					$.get("new_user.php?user="+usr.username+"&name="+usr.name+"&surname1="+usr.sur1+"&surname2="+usr.sur2+"&day="+usr.day+"&month="+usr.month+"&year="+usr.year+"&born="+usr.born+"&spirituality="+usr.feeling+"&user_id="+usr.user_id, muestraResultadoNewUser, "json");
					function muestraResultadoNewUser(resultado){
						if (resultado.success==1){
							console.log(resultado.msg);
							window.open("http://localhost:8888/numprojectclassic/dashboard.php","_self")
						} else {
							console.log(resultado.msg);
						}
					}
				}, 1500);
			
			} else {
				console.log(formResult);
			} 

			/*
			$('.content-popup').css("box-shadow", "none");
			$('.popup-overlay').css("opacity", 0.9);
			$('.popup-overlay').fadeIn('slow');
	        $('.popup-overlay').height($(document).height());
	        $('#popup').show();
			$('.preloader-popup').show();

			*/

			return false;

		});
	});
</script>
</head>
<body>
	<header> <span class="logo">APP - Lectura de tu Carta Numerológica</span> 
    <span class="menu"> 
    <?php
      $auth_url = url($client_id, $redirect_url, $scopes);
      if(isset($_SESSION['user'])) { 
        echo '<a href="includes/logout.php"><button class="log-in">LOGOUT</button></a>'; 
      }

      else { 
        echo "<a href='$auth_url'><button class='log-in'>LOGIN</button></a>"; 
      }
    ?>
    </span>
  </header>
   <div class="container">
      <div class="col-md-6 mx-auto text-center">
         <div class="header-title">
            <h1 class="wv-heading--title" style="color: #8da1ad">
               	Tu Carta Numerológica <br> ¡es gratuita! <!--<?php //echo $_SESSION['name'] ?> <?php // echo $_SESSION['user_id'] ?>--><br>
            </h1>
            <h3>(Si sigues el Directo hasta el final)</h3>
            <h2 class="wv-heading--subtitle">
               Anota tus datos personales, nombre <b>completo</b>, tus dos apellidos (1º paterno y 1º materno) y tu fecha de nacimiento.
            </h2>
         </div>
      </div>
      <div class="row">
         <div class="col-md-4 mx-auto">
            <div class="myform form ">
            	
               <form action="" method="post" name="login">
                  <div class="form-group">
                     <input type="text" name="your-username" size="40"  class="form-control my-input" placeholder="Username: <?php echo $_SESSION['username'] ?>" value='<?php echo $_SESSION['username'] ?>' disabled="disabled">
                  </div>
                  <div class="form-group">
                     <input type="hidden" name="your-userid" size="40"  class="form-control my-input" value="<?php echo $_SESSION['user_id'] ?>" disabled="disabled">
                  </div>
                  <div class="form-group">
                     <input type="text" name="your-name" size="40"  class="form-control my-input" id="name" placeholder="Nombre completo">
                  </div>
                  <div class="form-group">
                     <input type="text" name="your-surname1" value="" size="40"  class="form-control my-input" id="email" placeholder="Primer apellido">
                  </div>
                  <div class="form-group">
                     <input type="text" name="your-surname2" value="" size="40"  class="form-control my-input" placeholder="Segundo apellido">
                  </div>
                  <div class="form-group">
					<select name="Born" id="countrySelect" class="form-control my-input">
						<option value="" selected>NACIDO EN*</option>
				        <option value="es">España</option>
				        <option value="mx">México</option>
				        <option value="ar">Argentina</option>
				        <option value="ch">Chile</option>
				        <option value="pe">Perú</option>
				        <option value="co">Colombia</option>
				        <option value="gu">Guatemala</option>
				        <option value="ur">Uruguay</option>
				        <option value="pa">Paraguay</option>
				        <option value="bo">Bolivia</option>
				        <option value="ve">Venezuela</option>
				        <option value="ec">Ecuador</option>
				        <option value="ev">El Salvador</option>
				        <option value="be">Belice</option>
				        <option value="ho">Honduras</option>
				        <option value="ni">Nicaragua</option>
				        <option value="cr">Costa Rica</option>
				        <option value="pnm">Panamá</option>
				        <option value="ru">Rumanía</option>
				        <option value="us">Estados Unidos</option>
				        <option value="uk">Reino Unido</option>
				        <option value="fr">Francia</option>
				        <option value="it">Italia</option>
				        <option value="ot">Otros</option>
				    </select>
				</div>

				<div class="form-group">
					<select name="Day" class="form-control my-input">
					  <option value="" selected>DÍA*</option>
				      <option value="01">1</option>
				      <option value="02">2</option>
				      <option value="03">3</option>
				      <option value="04">4</option>
				      <option value="05">5</option>
				      <option value="06">6</option>
				      <option value="07">7</option>
				      <option value="08">8</option>
				      <option value="09">9</option>
				      <option value="10">10</option>
				      <option value="11">11</option>
				      <option value="12">12</option>
				      <option value="13">13</option>
				      <option value="14">14</option>
				      <option value="15">15</option>
				      <option value="16">16</option>
				      <option value="17">17</option>
				      <option value="18">18</option>
				      <option value="19">19</option>
				      <option value="20">20</option>
				      <option value="21">21</option>
				      <option value="22">22</option>
				      <option value="23">23</option>
				      <option value="24">24</option>
				      <option value="25">25</option>
				      <option value="26">26</option>
				      <option value="27">27</option>
				      <option value="28">28</option>
				      <option value="29">29</option>
				      <option value="30">30</option>
				      <option value="31">31</option>
				    </select>
				</div>

				<div class="form-group">
					<select name="month" class="form-control my-input">
						<option value="" selected>MES*</option>
				        <option value="01">Enero</option>
				        <option value="02">Febrero</option>
				        <option value="03">Marzo</option>
				        <option value="04">Abril</option>
				        <option value="05">Mayo</option>
				        <option value="06">Junio</option>
				        <option value="07">Julio</option>
				        <option value="08">Agosto</option>
				        <option value="09">Septiembre</option>
				        <option value="10">Octubre</option>
				        <option value="11">Noviembre</option>
				        <option value="12">Diciembre</option>
				    </select>
				</div>

				<div class="form-group">
					<select name="Year" class="form-control my-input">
					  <option value="" selected>AÑO*</option>
				      <option value="2030">2030</option>
				      <option value="2029">2029</option>
				      <option value="2028">2028</option>
				      <option value="2027">2027</option>
				      <option value="2026">2026</option>
				      <option value="2025">2025</option>
				      <option value="2024">2024</option>
				      <option value="2023">2023</option>
				      <option value="2022">2022</option>
				      <option value="2021">2021</option>
				      <option value="2020">2020</option>
				      <option value="2019">2019</option>
				      <option value="2018">2018</option>
				      <option value="2017">2017</option>
				      <option value="2016">2016</option>
				      <option value="2015">2015</option>
				      <option value="2014">2014</option>
				      <option value="2013">2013</option>
				      <option value="2012">2012</option>
				      <option value="2011">2011</option>
				      <option value="2010">2010</option>
				      <option value="2009">2009</option>
				      <option value="2008">2008</option>
				      <option value="2007">2007</option>
				      <option value="2006">2006</option>
				      <option value="2005">2005</option>
				      <option value="2004">2004</option>
				      <option value="2003">2003</option>
				      <option value="2002">2002</option>
				      <option value="2001">2001</option>
				      <option value="2000">2000</option>
				      <option value="1999">1999</option>
				      <option value="1998">1998</option>
				      <option value="1997">1997</option>
				      <option value="1996">1996</option>
				      <option value="1995">1995</option>
				      <option value="1994">1994</option>
				      <option value="1993">1993</option>
				      <option value="1992">1992</option>
				      <option value="1991">1991</option>
				      <option value="1990">1990</option>
				      <option value="1989">1989</option>
				      <option value="1988">1988</option>
				      <option value="1987">1987</option>
				      <option value="1986">1986</option>
				      <option value="1985">1985</option>
				      <option value="1984">1984</option>
				      <option value="1983">1983</option>
				      <option value="1982">1982</option>
				      <option value="1981">1981</option>
				      <option value="1980">1980</option>
				      <option value="1979">1979</option>
				      <option value="1978">1978</option>
				      <option value="1977">1977</option>
				      <option value="1976">1976</option>
				      <option value="1975">1975</option>
				      <option value="1974">1974</option>
				      <option value="1973">1973</option>
				      <option value="1972">1972</option>
				      <option value="1971">1971</option>
				      <option value="1970">1970</option>
				      <option value="1969">1969</option>
				      <option value="1968">1968</option>
				      <option value="1967">1967</option>
				      <option value="1966">1966</option>
				      <option value="1965">1965</option>
				      <option value="1964">1964</option>
				      <option value="1963">1963</option>
				      <option value="1962">1962</option>
				      <option value="1961">1961</option>
				      <option value="1960">1960</option>
				      <option value="1959">1959</option>
				      <option value="1958">1958</option>
				      <option value="1957">1957</option>
				      <option value="1956">1956</option>
				      <option value="1955">1955</option>
				      <option value="1954">1954</option>
				      <option value="1953">1953</option>
				      <option value="1952">1952</option>
				      <option value="1951">1951</option>
				      <option value="1950">1950</option>
				      <option value="1949">1949</option>
				      <option value="1948">1948</option>
				      <option value="1947">1947</option>
				      <option value="1946">1946</option>
				      <option value="1945">1945</option>
				      <option value="1944">1944</option>
				      <option value="1943">1943</option>
				      <option value="1942">1942</option>
				      <option value="1941">1941</option>
				      <option value="1940">1940</option>
				      <option value="1939">1939</option>
				      <option value="1938">1938</option>
				      <option value="1937">1937</option>
				      <option value="1936">1936</option>
				      <option value="1935">1935</option>
				      <option value="1934">1934</option>
				      <option value="1933">1933</option>
				      <option value="1932">1932</option>
				      <option value="1931">1931</option>
				      <option value="1930">1930</option>
				      <option value="1929">1929</option>
				      <option value="1928">1928</option>
				      <option value="1927">1927</option>
				      <option value="1926">1926</option>
				      <option value="1925">1925</option>
				      <option value="1924">1924</option>
				      <option value="1923">1923</option>
				      <option value="1922">1922</option>
				      <option value="1921">1921</option>
				      <option value="1920">1920</option>
				      <option value="1919">1919</option>
				      <option value="1918">1918</option>
				      <option value="1917">1917</option>
				      <option value="1916">1916</option>
				      <option value="1915">1915</option>
				      <option value="1914">1914</option>
				      <option value="1913">1913</option>
				      <option value="1912">1912</option>
				      <option value="1911">1911</option>
				      <option value="1910">1910</option>
				      <option value="1909">1909</option>
				      <option value="1908">1908</option>
				      <option value="1907">1907</option>
				      <option value="1906">1906</option>
				      <option value="1905">1905</option>
				      <option value="1904">1904</option>
				      <option value="1903">1903</option>
				      <option value="1902">1902</option>
				      <option value="1901">1901</option>
				      <option value="1900">1900</option>
				    </select>
				</div>

				<div class="form-group">
					<select name="feeling" class="form-control my-input">
						<option value="" selected>Me considero una persona *</option>
				        <option value="01">Nada espiritual</option>
				        <option value="02">Muy poco espiritual</option>
				        <option value="03">Un poco espiritual</option>
				        <option value="04">Espiritual</option>
				        <option value="05">Muy espiritual</option>
				    </select>
				</div>

                  <div class="text-center ">
                     <button id="register" class="btn btn-block send-button tx-tfm">¡Registrarme!</button>
                  </div>
                  <!--
                  <div class="col-md-12 ">
                     <div class="login-or">
                        <hr class="hr-or">
                        <span class="span-or">or</span>
                     </div>
                  </div>
                  <div class="form-group">
                     <a class="btn btn-block g-button" href="#">
                     <i class="fa fa-google"></i> Sign up with Google
                     </a>
                  </div>
              -->
                  <p class="small mt-3 acceptance-label"><input type="checkbox" name="acceptance" value="0" aria-invalid="false"/> He leído y acepto los <a href="#" class="ps-hero__content__link">Términos de Uso</a> y la <a href="#">Política de Privacidad</a>.</p>
               </form>
            </div>
         </div>
      </div>
   </div>

   		<!-- Popup Loading Register -->
   		<div id="popup" style="display: none;">
   			<!--
		    <div class="content-popup">
		        <div>
		        	<h2 id="popup-title">¿Estás suscrito al Canal?</h2>
		            <p id="popup-description">Para utilizar la calculadora gratuita <u>debes estar suscrito</u> al Canal de Practicante Espiritual.</p>
		            <div id="logo-popup" class="logo-popup"><img class="logo_image" src="images/logo_PE_circle_200.png" srcset="images/logo_PE_circle_200.png 2x">
		            	<div id="button-yt-subscribe" class="btn-1">
						  <a href="#" class="button button-blue">¡Verificar!</a>
						</div>
		            </div>
		    -->
					<div class="preloader-popup">
					  <img src="images/spinner.svg" alt="spinner">
					</div>
					<div class="preloader-popup">
					  Poniéndote a la cola...
					</div>
			<!--

		    	</div>
		        </div>
		    </div>
			-->
		</div>

		<div class="popup-overlay"></div>


</body>
</html>

