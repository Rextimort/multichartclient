<?php
/* Form Page
* Formulario para solicitar tu Carta Numerológica.
* @author : Gonzalo Pareja Bolaños
* @copyright : Youtube Channel: Practicante Espiritual
*/
session_start();
include("conex.php");
$link=Conectarse();

# Enabling error display
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

# Including all the required scripts for demo
require __DIR__ . "/includes/functions.php";
require __DIR__ . "/includes/discord.php";
require __DIR__ . "/config.php";

if (isset($_SESSION['user_id']) != ''){
  if (isset($_SESSION['name']) == ''){
    header('Location: '.$server_url.'form.php');
  }
} else {
  header('Location: '.$server_url.'index.php');
}
?>
<!DOCTYPE html>
<html>
<head>
  <title></title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.js"></script>

<!------ Include the above in your HEAD tag ---------->

<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

<link href="css/table.css" rel="stylesheet">
<link rel="stylesheet" href="assets/css/style.css">

<script>
jQuery(document).ready(function($){
    $("#editData").click(function(){
      window.open("http://localhost:8888/numprojectclassic/form.php?edit_data=1","_self")
    });
    
    var user_id = '<?php echo $_SESSION["user_id"]; ?>';
    $.get("get_current_pos.php?user_id="+user_id, muestraResultadoPosUser, "json");

    var intervalId = window.setInterval(function(){ 
      
       $.get("get_current_pos.php?user_id="+user_id, muestraResultadoPosUser, "json");
                    
    }, 5000);


    function muestraResultadoPosUser(resultado){
            
      console.log(resultado)
        
        if (resultado.personsInLine>0){
          $("#alerts").html("Tienes <h1>"+resultado.personsInLine+"</h1> personas delante de ti.");
        } else{
          $("#alerts").html("<h2>¡Gonzalo está leyendo tu Carta ahora mismo!</h2>");
        }

        if (resultado.link_enabled==1){
          $("#link").html('<span style="cursor:pointer" class="link-personal" onclick="charting('+"'"+resultado.random_code+"'"+')">Descargar</span>');
        } else {
          $("#link").html('');
        }
    }  



});

function charting(code){
      console.log(code);
      window.open("http://localhost:8888/numprojectclassic/yourchart.php?chartcode="+code);
}

</script>
</head>
<body>

<header> <span class="logo">APP - Lectura de tu Carta Numerológica</span> 
  <span class="menu"> 
  <?php
      echo '<a href="includes/logout.php"><button class="log-in">LOGOUT</button></a>';   
  ?>
  </span>
</header>

<h1>Dashboard <span>1.0</span> de <span>Numerología</span></h1>
<br>
<h1><span>Bienvenido</span> <?php echo $_SESSION["username"]; ?></h1>
<h2>Has solicitado la Carta Numerológica para:</h2>

<table id="users" class="responstable">
  
  <tr>
    <th>Usuario</th>
    <th data-th="Tus datos"><span>Nombre</span></th>
    <th>Apellidos</th>
    <th>Fecha de nacimiento</th>
    <th>País de nacimiento</th>
    <th>Espiritualidad</th>
    <th>Link</th>
  </tr>
  <tr>
    <td><?php echo $_SESSION["username"]; ?></td>
    <td><?php echo $_SESSION["name"]; ?></td>
    <td><?php echo $_SESSION["surname1"]." ".$_SESSION["surname2"]; ?></td>
    <td><?php echo $_SESSION["birthdate"]; ?></td>
    <td><?php echo strtoupper($_SESSION["country"]); ?></td>
    <td><?php echo $_SESSION["spirituality"]; ?></td>
    <td><div id="link"></div></td>
  </tr>
  
</table>

<br>Si estos datos no fuesen correctos, o quisiéras hacérsela a otra persona, haz clic <span id="editData">aquí</span> para volver a introducirlos.
<br><br>
<span id="alerts"></span>

</body>
</html>