<?php

/* Home Page
* The home page of the working demo of oauth2 script.
* @author : MarkisDev
* @copyright : https://markis.dev
*/

# Enabling error display
error_reporting(E_ALL);
ini_set('display_errors', 1);


# Including all the required scripts for demo
require __DIR__ . "/includes/functions.php";
require __DIR__ . "/includes/discord.php";
require __DIR__ . "/config.php";

# ALL VALUES ARE STORED IN SESSION!
# RUN `echo var_export([$_SESSION]);` TO DISPLAY ALL THE VARIABLE NAMES AND VALUES.
# FEEL FREE TO JOIN MY SERVER FOR ANY QUERIES - https://join.markis.dev

?>

<html>
<head>
  <title>APP Numerología</title>
  <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
  <header> <span class="logo">APP - Lectura de tu Carta Numerológica</span> 
    <span class="menu"> 
    <?php
      $auth_url = url($client_id, $redirect_url, $scopes);
      if(isset($_SESSION['user'])) { 
        echo '<a href="includes/logout.php"><button class="log-in">LOGOUT</button></a>'; 
      }

      else { 
        echo "<a href='$auth_url'><button class='log-in'>LOGIN</button></a>"; 
      }
    ?>
    </span>
  </header>
  
</body>
</html>